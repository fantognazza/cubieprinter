setenv bootargs root=/dev/mmcblk0p1 rw rootfstype=ext4 rootwait panic=10 initrd=/boot/initramfs-linux.img console=ttyS0,115200
ext4load mmc 0:1 0x43000000 boot/dtbs/sun4i-a10-cubieboard.dtb
ext4load mmc 0:1 0x42000000 boot/zImage
bootz 0x42000000 - 0x43000000