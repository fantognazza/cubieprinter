# Cubieboard

## Purpose

This SBC is in charge of running the octoprint print service in order to manage the 3D printer and provide more features.

## Files

- **boot**: some files required to the device boot (up-to-date u-boot bootloader, boot.cmd, etc)
- **dtb**: contains all the tools needed to retrieve and patch the cubieboard DTB file
- **utility**: directory containing some helper script
