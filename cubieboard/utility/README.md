# Utility

## Files

- **3dctl**: utility that manages the GPIO pins connected to a simple 2-ports relay (must be put in _/usr/bin/_)
- **3dupload**: script that flashes the firmware to the atmel 1284p microcontroller
- **octoprint.service**: systemd service that starts the octoprint service (must be put in _/etc/systemd/system/_ and enabled)
- **sudoctoprint**: sudoers file that allows the execution of the main programs without interactive authentication (must be put in _/etc/sudoers.d/_)
