#!/bin/sh

# clean working directory
rm -rf include
rm -rf *.dts
rm -rf *.dtsi

# fetch needed libraries
wget --no-clobber --convert-links -r -p -E -e robots=off -U mozilla -I pub/scm/linux/kernel/git/stable/linux.git/plain/include/dt-bindings https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/plain/include/dt-bindings

mv git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/plain/include .

# get the device tree sources and dependencies
wget https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/plain/arch/arm/boot/dts/sun4i-a10-cubieboard.dts
wget https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/plain/arch/arm/boot/dts/sun4i-a10.dtsi
wget https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/plain/arch/arm/boot/dts/sunxi-common-regulators.dtsi
wget https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/plain/arch/arm/boot/dts/axp209.dtsi

# resolve the C style macros
# more info at https://elinux.org/Device_Tree_Linux
cpp -nostdinc -I ./ -I include -undef -x assembler-with-cpp sun4i-a10.dtsi > /tmp/sun4i-a10.dtsi
cpp -nostdinc -I ./ -I include -undef -x assembler-with-cpp sun4i-a10-cubieboard.dts > /tmp/sun4i-a10-cubieboard.dts

# compile the DTB
dtc -O dtb -o sun4i-a10-cubieboard.dtb /tmp/sun4i-a10-cubieboard.dts
