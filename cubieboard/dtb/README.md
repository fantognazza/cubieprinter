# Device Tree

The purpose of the DTB file is to provide a description of the peripherals present in the system. This file is parsed at each boot by the Linux kernel to correctly configure the system.

The SBC peripherals description is not always static. Therefore, in order to expose a particular peripheral to the I/O pins, an adaption of the DTS file is required.

For further info, visit https://elinux.org/Device_Tree_Linux.

## Purpose

All the following files are specific for the **cubieboard v1** SBC.
In particular, the aim is to expose a second UART peripheral  (**uart6**) to I/O pins (_PI12_ and _PI13_ as stated in the device datasheet).
As those pins overlap with the **spi0** peripheral, is therefore needed to disable it.

## Files

- **fetch_latest_dts.sh**: fetch latest DTS files directly from the Linux repository
- **cubieprinter.patch**: patch of DTS files in order to enable _uart6_ and disable _spi0_
- **A**: directory containing the full original DTS files
- **B**: directory containing the full modified DTS files
