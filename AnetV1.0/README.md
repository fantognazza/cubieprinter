# Anet V1.0

## Purpose

This microcontroller is in charge of running the Marlin firmware.
Some hardware modifications were applied (removed embedded USB-UART connection, exposed unused I/O pins, etc)

## Files

- **Marlin**: Marlin configuration used by the firmware
- **toolhead**: electrical documentation of custom universal connectors that can be used for FDM, CNC and laser cut toolheads.
