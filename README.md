# CubiePrinter

## Purpose

This repository is a collection of tools, configurations, patch and documents related to the 3D print.

## Files

- **cubieboard**: contains all the files related to the SBC running the octoprint service, starting from DTS patch to systemd service files.
- **microcontroller**: contains configurations, schematics and mods of the 3D printer microcontroller board
